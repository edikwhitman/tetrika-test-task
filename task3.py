from datetime import datetime
import time


# входит ли данное время t в один из интервалов массива
def exists(intervals, t):
    if t < intervals[0] or t > intervals[-1]:
        return False
    for x in range(0, len(intervals), 2):
        if intervals[x] <= t < intervals[x+1]:
            return True
        x += 2
    return False


#  более медленное решение (более чем в 2 раза больше времени занимает, чем второе)
def appearance1(intervals):
    count = 0
    #  проходим по каждой секунде урока и проверяем, что в эту секунду на уроке был и ученик и учитель
    for t in range(intervals['lesson'][0], intervals['lesson'][1]):
        if exists(intervals['tutor'], t) and exists(intervals['pupil'], t):
            count += 1

    return count


#  возвращает True, если два интервала не соприкасаются
def not_touch(a1, a2, b1, b2):
    return a1 > b2 or a2 < b1


def appearance(intervals):
    count = 0
    p = 0
    # объединяем пересекающиеся промежутки, чтобы избежать повторения частей интервалов (чтобы не считать их дважды)
    while p < len(intervals['pupil'])-2:
        if not not_touch(intervals['pupil'][p], intervals['pupil'][p+1], intervals['pupil'][p+2], intervals['pupil'][p+3]):
            dots = sorted([intervals['pupil'][p], intervals['pupil'][p+1], intervals['pupil'][p+2], intervals['pupil'][p+3]])
            intervals['pupil'].pop(p+2)
            intervals['pupil'].pop(p+2)
            intervals['pupil'][p] = dots[0]
            intervals['pupil'][p+1] = dots[3]
        else:
            p += 2
    t = 0
    start = intervals['lesson'][0]
    end = intervals['lesson'][1]
    # обрезаем интервалы учителя, в соответствии с началом и концом урока, удаляем лишние
    while t < len(intervals['tutor']):
        if not_touch(intervals['tutor'][t], intervals['tutor'][t+1], start, end):
            intervals['tutor'].pop(t)
            intervals['tutor'].pop(t+1)
            continue
        if intervals['tutor'][t] < start:
            intervals['tutor'][t] = start
        if intervals['tutor'][t+1] > end:
            intervals['tutor'][t+1] = end
        t += 2
    p, t = 0, 0
    # проходим по интервалам ученика и учителя, считая пересечения
    while t < len(intervals['tutor']):
        stop = False
        while p < len(intervals['pupil']) and not stop:
            if not not_touch(
                    intervals['tutor'][t], intervals['tutor'][t + 1], intervals['pupil'][p], intervals['pupil'][p+1]):
                dots = sorted(
                    [intervals['tutor'][t], intervals['tutor'][t + 1], intervals['pupil'][p + 1], intervals['pupil'][p]])
                count += dots[2] - dots[1]
            if intervals['pupil'][p+1] > intervals['tutor'][t+1]:
                stop = True
            if intervals['pupil'][p+1] < intervals['tutor'][t+1]:
                p += 2

        t += 2

    return count


tests = [
    {'data': {'lesson': [1594663200, 1594666800],
              'pupil': [1594663340, 1594663389, 1594663390, 1594663395, 1594663396, 1594666472],
              'tutor': [1594663290, 1594663430, 1594663443, 1594666473]},
     'answer': 3117
     },
    {'data': {'lesson': [1594702800, 1594706400],
              'pupil': [1594702789, 1594704500, 1594702807, 1594704542, 1594704512, 1594704513, 1594704564, 1594705150,
                        1594704581, 1594704582, 1594704734, 1594705009, 1594705095, 1594705096, 1594705106, 1594706480,
                        1594705158, 1594705773, 1594705849, 1594706480, 1594706500, 1594706875, 1594706502, 1594706503,
                        1594706524, 1594706524, 1594706579, 1594706641],
              'tutor': [1594700035, 1594700364, 1594702749, 1594705148, 1594705149, 1594706463]},
     'answer': 3577
     },
    {'data': {'lesson': [1594692000, 1594695600],
              'pupil': [1594692033, 1594696347],
              'tutor': [1594692017, 1594692066, 1594692068, 1594696341]},
     'answer': 3565
     },
]

if __name__ == '__main__':
    for i, test in enumerate(tests):
        #  оставил 2 решения, одно - в лоб, очень медленное, но легко читаемое,
        #  второе более быстрое, но сложнее в понимании
        test_answer = appearance(test['data'])
        test_answer1 = appearance1(test['data'])

        print(datetime.now() - start_time)
        assert test_answer == test['answer'], f'Error on test case {i}, got {test_answer}, expected {test["answer"]}'
        assert test_answer1 == test['answer'], f'Error on test case {i}, got {test_answer}, expected {test["answer"]}'
