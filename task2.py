import requests
from bs4 import BeautifulSoup as bs


def init(animals_dict):
    alphabet = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    for letter in alphabet:
        animals_dict[letter] = 0


def main():
    url_base = "https://ru.wikipedia.org"
    next_page = [{'href': "/w/index.php?title=%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D1%8F:%D0%96%D0%B8%D0%"
                          "B2%D0%BE%D1%82%D0%BD%D1%8B%D0%B5_%D0%BF%D0%BE_%D0%B0%D0%BB%D1%84%D0%B0%D0%B2%D0%B8%D1%82%D1%"
                          "83&pageuntil=%D0%90%D0%B7%D0%B8%D0%B0%D1%82%D1%81%D0%BA%D0%B8%D0%B9+%D0%BC%D1%83%D1%80%D0%B0"
                          "%D0%B2%D0%B5%D0%B9-%D0%BF%D0%BE%D1%80%D1%82%D0%BD%D0%BE%D0%B9#mw-pages"}]
    url_template = url_base + next_page[0]['href']
    animals = {}
    init(animals)
    i = 1
    stop = False
    while next_page and not stop:
        url_template = url_base + next_page[0]['href']
        r = requests.get(url_template)
        soup = bs(r.text, "html.parser")
        div_animals = soup.find_all('div', class_='mw-category mw-category-columns')

        for name in div_animals:
            for animal in name.ul:
                if animal.text != '\n':
                    if 'А' <= animal.text[0] <= 'Я' or animal.text[0] == 'Ё':
                        animals[animal.text[0]] += 1
                    else:
                        stop = True
        print("page", i, "loading...")
        next_page = soup.find_all('a', string="Следующая страница")
        i += 1

    for letter, count in animals.items():
        print(letter + ':', count)


if __name__ == '__main__':
    main()
